## Credentials received from terraform.tfvars file

variable "ak" {}
variable "sk" {}

## Region and Availability zone variables ##

variable "region" {
#    default = "la-south-2"
#    default = "sa-brazil-1"
#    default = "na-mexico-1"
    default = "la-north-2"
}

variable "availability_zone1" {
#    default = "la-south-2a"
#    default = "sa-brazil-1a"
#    default = "na-mexico-1a"
    default = "la-north-2a"
}

variable "availability_zone2" {
#    default = "la-south-2b"
#    default = "sa-brazil-1b"
#    default = "na-mexico-1a"
    default = "la-north-2a"
}

## Network variables ##

variable "vpc_name" {
    default = "vpc_moodle"
}

variable "vpc_cidr" {
    default = "10.10.0.0/16"
}

variable "subnet_name" {
    default = "subnet_moodle"
}

variable "subnet_cidr" {
    default = "10.10.1.0/24"
}

variable "subnet_gateway_ip" {
    default = "10.10.1.1"
}

## Application Environment variables ## 
variable "app_name" {
    default = "moodle"
}

variable "environment" {
  default = "production"
}

variable "tags"{
  default = {
    Name        = "moodle"
    Environment    = "production"
    Backup      = "true"
  }
}

## ECS Instance variables ## 

variable "ecs_image_id" {
  default = "5432a87d-b28f-41fb-aa10-9802b049fc39"
##MexicoCity2 LAMP-Debian10-Apache2-Php7.4-MySQL5.7-v0.1##
}

variable "ecs_image_type" {
  default = "private"
}


variable "ecs_instance_type" {
  default = "s6.medium.2"
}

variable "ecs_sysdisk_type" {
  default = "GPSSD"
}

variable "ecs_sysdisk_size" {
  default = "40"
}

variable "ecs_datadisk_type" {
  default = "GPSSD"
}

variable "ecs_datadisk_size" {
  default = "100"
}

variable "ecs_instance_id" {
  default = ""
}

variable "public_key" {
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDy+49hbB9Ni2SttHcbJU+ngQXUhiGDVsflp2g5A3tPrBXq46kmm/nZv9JQqxlRzqtFi9eTI7OBvn2A34Y+KCfiIQwtgZQ9LF5ROKYsGkS2o9ewsX8Hghx1r0u5G3wvcwZWNctgEOapXMD0JEJZdNHCDSK8yr+btR4R8Ypg0uN+Zp0SyYX1iLif7saiBjz0zmRMmw5ctAskQZmCf/W5v/VH60fYPrBU8lJq5Pu+eizhou7nFFDxXofr2ySF8k/yuA9OnJdVF9Fbf85Z59CWNZBvcTMaAH2ALXFzPCFyCncTJtc/OVMRcxjUWU1dkBhOGQ/UnhHKcflmrtQn04eO8xDr root@terra-dev"
}

## Security Group variable ## 

variable "ports-ranges" {
  description = "Port ranges to create on security group rule"
  default = ["80" , "443"]
}

variable "remote_ip_prefix" {
  default = "0.0.0.0/0"
}

## Elastic IP variables ##

variable "ecs_attach_eip" {
  default = true
}

variable "eip_bandwidth_size" {
  default = "50"
}


## CTS variable ##
variable "cts_transfer_bucket" {
    default = ""
}

## CBR variable ##
variable "cbr_server_policy" {
    default = "server-bk-policy"
}

variable "cbr_server_vault_name" {
    default = "server-bk-vault"
}

variable "cbr_server_vault_size" {
    default = "100"
}

## SMN variable ##
variable "alarm_email_address" {
    default = "abcd@abc.com"
}
